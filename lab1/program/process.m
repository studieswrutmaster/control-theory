function [dx] = process(t,x,u)
%% procces
dx = zeros(3,1);
dx(1) =t;
dx(2) =x(1)+u(1);
dx(3) =t^2+u(2);

end