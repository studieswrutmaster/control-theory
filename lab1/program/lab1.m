function lab1
%% clear and close all
clear all
close all
clc

%% initial conditions and options
tspan = [0 10];
x0 = [1 2 3];
u = [1 2];
options = odeset('RelTol',1e-6,'AbsTol',1e-6);

%% solve process
equation = @(t,x)process(t,x,u);
[T,X] = ode45(equation,tspan,x0,options);

%% vizualization
plot(T,X);
legend('x_1','x_2','x_3',3);
xlabel('time (s)');
ylabel('x');
grid on;

end