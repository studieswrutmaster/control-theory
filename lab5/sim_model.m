function [last_conditions,T,X] = sim_model(u,t0,tF,x0)

tspan = [t0 tF];

equation = @(t,x)model(t,x ,u);
[T,X] = ode45(equation,tspan,x0);


% performance_index = X(end,3)*X(end,4);
last_conditions = X(end,1:4);

% figure
% plot(T,X)
% legend('x_1','x_2','x_3','x_4',2);
% xlabel('time (s)');
% ylabel('x');
% grid on;

end