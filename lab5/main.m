clc 
clear all
close all

%% initial parameters
u_values = [7 9 10 8 7 4];
t_values = [1 5 10 15 20 25];
t0 = 0;

%% computation
% % dt = tF/length(u_values);
% tF = dt;


x0 = [1 150 0 10];
penalty = 10^3;


T_vect = [];
X_vect = [];
u_vect = [];

for i = 1:length(t_values)
    u = u_values(i);
    tF = t_values(i);
    [x0,T,X] = sim_model(u,t0,tF,x0);
    t0 = tF;
    if i < length(t_values)
        tF = t_values(i+1);
    end
    u_vect = [u_vect u*ones(1,length(T))];
    T_vect = [T_vect T'];
    X_vect = [X_vect X'];
end

%% performance index
if X_vect(4,end) > 200
    performance_index = X_vect(3,end)*X_vect(4,end) - penalty*(X_vect(4,end)-200)^2;
else 
    performance_index = X_vect(3,end)*X_vect(4,end);
end

% performance_index = performance_index/1000 % kg

%% vizualization
figure('Position', [0, 0, 1000, 400])
subplot(1,2,1)
plot(T_vect',u_vect);
xlabel('time (h)');
ylabel('u');
axis([0 t_values(length(t_values)) 0 12])
subplot(1,2,2)
plot(T_vect',X_vect');
title(sprintf('J = %.0f (g)',performance_index))
legend('x_1','x_2','x_3','x_4',3);
xlabel('time (h)');
ylabel('x');
grid on

