clc
clear all
close all

index = [];
steps = [];
i = 1;
for u = 0:0.01:1
    index = [index control_process(u)];
    steps = [steps i];
    i = i + 1;
end

plot(0:0.01:1,index);
title('');
xlabel('control value u');
ylabel('performance index');
grid

