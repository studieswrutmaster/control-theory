function [dx] = catalyst(t,x,u)

dx = zeros(2,1);
dx(1) = u*(10*x(2)-x(1));
dx(2) = u*(x(1)-10*x(2))-(1-u)*x(2);

end