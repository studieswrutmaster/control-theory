\chapter{Implementation in MATLAB}

\begin{enumerate}

\item 
\begin{lstlisting}[style=customc]
function [dx] = catalyst(t,x,u)
     dx = zeros(2,1);
     dx(1) = u*(10*x(2)-x(1));
     dx(2) = u*(x(1)-10*x(2))-(1-u)*x(2);
end
\end{lstlisting}
where:
\begin{itemize}
\item \verb$process(t,x,u)$ -- the name of the function and its arguments,
\item \verb$dx$ -- the vector of the right hand side of the equation.
\end{itemize}

\item 
\begin{lstlisting}[style=customc]
function [performance_index] = control_process(u)
    %% initial conditions and options
    tspan = [0 1];
    x0 = [1 0];
    options = odeset('RelTol',1e-6,'AbsTol',1e-6);

    %% solve process
    equation = @(t,x)catalyst(t,x,u);
    [T,X] = ode45(equation,tspan,x0,options);

    x = 1-X(:,1)-X(:,2);
    performance_index = x(end);
end
\end{lstlisting}
where:
\begin{itemize}
\item \verb$process(t,x,u)$ -- the name of the function and its arguments,
\item \verb$dx$ -- the vector of the right hand side of the equation.
\end{itemize}

\item 
\begin{lstlisting}[style=customc]
clc
clear all
close all

index = [];
steps = [];
i = 1;
for u = 0:0.01:1
    index = [index control_process(u)];
    steps = [steps i];
    i = i + 1;
end

plot(0:0.01:1,index);
title('');
xlabel('control value u');
ylabel('performance index');
grid
\end{lstlisting}
where:
\begin{itemize}
\item \verb$process(t,x,u)$ -- the name of the function and its arguments,
\item \verb$dx$ -- the vector of the right hand side of the equation.
\end{itemize}

\end{enumerate}