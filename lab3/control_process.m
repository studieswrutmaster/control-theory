%Control Theory lab3 
function [performance_index] = control_process(T,S,t1)
%% clear and close all
% clear all
% close all
% clc

%% parameters
tF = 180;   % min

%% control vector
u = [T S t1];

%% initial conditions and options for a fisrt reactor
tspan1 = [0 u(3)];
x01 = [2000 0 0 0 0 0];
options = odeset('RelTol',1e-6,'AbsTol',1e-6);

%% solve process for a first reactor
equation = @(t,x)reactor1(t,x,u);
[T1,C1] = ode45(equation,tspan1,x01,options);

%% vizualization for a fist reactor
% figure(1)
% plot(T1,C1);
% title('First reactor')
% legend('C_A','C_B','C_C',1);
% xlabel('time (min)');
% ylabel('C');
% grid on;

%% compute new concentration
V1 = 0.1; %m3
V2 = V1 + u(2); %m3
C_B0 = 600; %mol/m3
C_A = V1*C1(end,1)/V2; %mol/m3
C_B = (V1*C1(end,2)+S*C_B0)/V2; %mol/m3
C_C = V1*C1(end,3)/V2; %mol/m3
%% initial conditions and options for a second reactor
tspan2 = [u(3) tF];
x02 = [C_A C_B C_C 0 0 0];
options = odeset('RelTol',1e-6,'AbsTol',1e-6);
%% solve process for a second reactor
equation = @(t,x)reactor2(t,x,u);
[T2,C2] = ode45(equation,tspan2,x02,options);


%% vizualization for a second reactor
% figure(2);
% plot(T2,C2);
% title('Second reactor')
% legend('C_A','C_B','C_C',1);
% xlabel('time (min)');
% ylabel('C');
% grid on;

%% complex results
T = [T1; T2];
C = [C1; C2];

%% vizualization for complex results
figure;
plot(T,C(:,1:3).*V2);
title('Reaction stage 1')
legend('C_A','C_B','C_C',1);
xlabel('time (min)');
ylabel('V_2C (mol)');
grid on;

figure;
plot(T,C(:,4:6).*V2);
title('Reaction stage 2')
legend('C_D','C_E','C_F',4);
xlabel('time (min)');
ylabel('V_2C (mol)');
grid on;

performance_index = V2*C(end,4); %mol

end
