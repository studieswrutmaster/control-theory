function [dx] = reactor1(t,x,u)
k1 = 0.0444*exp(-2500/u(1)); %m3
k2 = 6889*exp(-5000/u(1)); %m3

dx = zeros(6,1);
dx(1) = -2*k1*x(1)^2;
dx(2) = k1*x(1)^2 - k2*x(2);
dx(3) = k2*x(2);
dx(4) = 0;
dx(5) = 0;
dx(6) = 0;

end