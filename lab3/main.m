clc
clear all
close all

control_process(312,0.07,106);

%% complex simulation
% T = 300;    % 298  < T(t) < 398 [K]
% S = 0.1;   % 0 < S <0.1 [m3]
% t1 = 115;   % 0 < t1 < 180 [min]
% 
% T = 298:398;
% t1 = 1:179;
% index = ones(length(t1),length(T));
% 
% for j = 1:length(T)
%     for i = 1:length(t1)
%         index(i,j) = control_process(T(j),S,t1(i));
%     end
% end
% 
% figure
% surf ( T , t1 , index )
% title(sprintf('S = %.1f',S))
% xlabel ( 'T (K) ')
% ylabel ( 't_1 (min)')
% zlabel ( 'V_2C_D (mol) ')
% xlim([298 398])
% ylim([1 179])
% 
% [value, location] = max(index(:));
% [R,C] = ind2sub(size(index),location);
% index(R,C)
% T(C)
% t1(R)

%% another simulation
% T = 300;    % 298  < T(t) < 398 [K]
% S = 0.07;   % 0 < S <0.1 [m3]
% t1 = 115;   % 0 < t1 < 180 [min]
% 
% index = [];
% 
% for T=298:398
%    index = [index control_process(T,S,t1)];
% end
% 
% T = 298:398;
% figure
% plot(T,index);
% grid
% 
% title(sprintf('t_1 = %d, S = %.2f',t1,S))
% xlabel('T (K)')
% ylabel('V_2C_D (mol)')
% xlim([298 398])
% 
% 
%% another simulation
% T = 300;    % 298  < T(t) < 398 [K]
% S = 0.07;   % 0 < S <0.1 [m3]
% t1 = 115;   % 0 < t1 < 180 [min]
% 
% index = [];
% 
% for t1=1:179
%    index = [index control_process(T,S,t1)];
% end
% 
% t1=1:179;
% figure
% plot(t1,index);
% grid
% 
% title(sprintf('T = %d, S = %.2f',T,S))
% xlabel('t_1 (min)')
% ylabel('V_2C_D (mol)')
% xlim([1 179])


%% another simulation
% T = 300;    % 298  < T(t) < 398 [K]
% S = 0.07;   % 0 < S <0.1 [m3]
% t1 = 115;   % 0 < t1 < 180 [min]
% 
% index = [];
% 
% for S=0:0.001:0.1
%    index = [index control_process(T,S,t1)];
% end
% 
% S=0:0.001:0.1;
% figure
% plot(S,index);
% grid
% 
% title(sprintf('T = %d, t1 = %d',T,t1))
% xlabel('S (m^3)')
% ylabel('V_2C_D (mol)')
% xlim([0 0.1])