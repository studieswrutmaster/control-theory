function performance_index = opt_process(input)

% input = [0.5 0.5 0.5 0.5 0.5 0.5 0.4 0.5 0.7 1 ];
intervals = (length(input)+2)/3;

t0 = 0;
tF = 1;
dt = 1/intervals;

%% initial conditions and options
u_vect = input(end+1-intervals:end);
x_vect = input(1:end-intervals);

x0 = [1 0];
x_vect = [x0, x_vect];

penalty = 10^3;

options = odeset('RelTol',1e-6,'AbsTol',1e-6);

T_all = [];
X_all = [];
U_all = [];
X_length = [0];
x0_all = [];

%% solve process
for i = 1:intervals
    u = u_vect(i);
    tspan = [t0 t0+dt];
    x0 = [x_vect(i*2-1) x_vect(i*2)];
    x0_all = [x0_all x0'];
    equation = @(t,x)catalyst(t,x,u);
    [T,X] = ode45(equation,tspan,x0,options);
    X = [X 1-X(:,1)-X(:,2)];
    X_length = [X_length X_length(end)+length(X)]; 
    T_all = [T_all T'];
    X_all = [X_all X'];
    U_all = [U_all ones(1,length(X))*u];
    
    t0 = t0+dt;
end

X_length = X_length(2:end);
X_length;

figure(1)
set(1,'position', [0, 0, 1000, 400],'PaperPositionMode', 'auto')
subplot(1,2,1)
plot(T_all,X_all)
title(sprintf('Performance index = %.4f',X_all(3,end)));
xlabel('time (s)')
ylabel('x')
legend(2,'x_1','x_2','x_3')
axis([0 1 0 1])
grid on

subplot(122)
plot(T_all,U_all)
xlabel('time (s)')
ylabel('u')
axis([0 1 0 1])
grid on

% performance_index = 1 - X_all(end,1)-X_all(end,2)

R = zeros(2*intervals-2,1);
    
for i = 1:intervals-1
    R(i*2-1,1) = x0_all(1,i+1) - X_all(1,X_length(i));
    R(i*2,1) = x0_all(2,i+1) - X_all(2,X_length(i));
end

performance_index =-(1-X_all(1,end)-X_all(2,end)) + penalty*norm(R)*norm(R);


end