function lab_8
    u0=[0.5 0.5 0.5 0.5 0.5 0.5 0.4 0.5 0.7 1 ];
 
%     opcje=optimset('Display','iter-detailed');
    par_opt=fminunc(@lab,u0)
end

function result = lab(x)
    ro=10000;

    tspan1=[0 0.25];   %time of the process in hours
    tspan2=[0.25 0.5 ];
    tspan3=[0.5 0.75];
    tspan4=[0.75  1.0];
    x01=[1 0];     %initial conditions
    u1=x(7);
    u2=x(8);
    u3=x(9);
    u4=x(10);
    options=odeset('RelTol',1e-6,'AbsTol',1e-6);    %setting ode accuracy

    [T1,X1]=ode45(@(t,x)model(t,x,u1),tspan1,x01,options);
    
    x02 = [x(1) x(2)];
     x03 = [x(3) x(4)];
      x04 = [x(5) x(6)];
    [T2,X2]=ode45(@(t,x)model(t,x,u2),tspan2,x02,options);

    [T3,X3]=ode45(@(t,x)model(t,x,u3),tspan3,x03,options);
    [T4,X4]=ode45(@(t,x)model(t,x,u4),tspan4,x04,options);

    plot(T1,X1,T2,X2,T3,X3,T4,X4)

    grid on;
    pause(0.1)
    
    R = zeros(6,1);
     R(1,1) = x(1) - X1(end,1);
     R(2,1) = x(2) - X1(end,2);
     
     R(3,1) = x(3) - X2(end,1);
     R(4,1) = x(4) - X2(end,2);
     
     R(5,1) = x(5) - X3(end,1);
     R(6,1) = x(6) - X3(end,2);
     
    result=-(1-X4(end,1)-X4(end,2)) + ro*norm(R)*norm(R);
end

function dx=model(~,x,u)
if u>1
    u=1;
end
if u<0
    u=0;
end
    dx=zeros(2,1);
    dx(1)=u*(10*x(2)-x(1));
    dx(2)=u*(x(1)-10*x(2))-(1-u)*x(2);
end
