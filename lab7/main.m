clc
clear all
close all

for intervals = [2,4,10,20,50];
    decision_values = ones(1,(intervals-1)*2+intervals)*0.5;

    opts=optimset('Display','iter-detailed','TolFun',10^-4);
    optimal = fminunc(@opt_process,decision_values,opts);

    print(sprintf('raport/chapter3/figures/%d',intervals),'-depsc')
    close all
end
