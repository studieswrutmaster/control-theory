%%  Control Theory lab2 Catalyst mixing problem
%% u between 0.0 and 1.0
function [performance_index] = control_process(u)
%% clear and close all
% clear all
% close all
% clc

%% initial conditions and options
tspan = [0 1];
x0 = [1 0];
options = odeset('RelTol',1e-6,'AbsTol',1e-6);

%% solve process
equation = @(t,x)catalyst(t,x,u);
[T,X] = ode45(equation,tspan,x0,options);

x = 1-X(:,1)-X(:,2);
performance_index = x(end);

%% vizualization
% plot(T,X,T,x);
% legend('x_1','x_2','x_3',2);
% xlabel('time (s)');
% ylabel('x');
% grid on;

end

