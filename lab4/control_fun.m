function [u] = control_fun(t,u_pam)

    a = u_pam(1);
    b = u_pam(2);
    c = u_pam(3);
    d = u_pam(4);
    e = u_pam(5);
    f = u_pam(6);
    g = u_pam(7);


    if t < 0.25
        u = a;
    elseif t < 0.5
        u = b;
    elseif t < 0.75
        u = c*t + d;
    else
        u = e*t^2 + f*t + g;
    end

    if u > 1 
        u = 1;
    elseif u < 0
        u = 0;
    end

end