%%  Control Theory lab2 Catalyst mixing problem
%% u between 0.0 and 1.0
function [performance_index] = control_process(u_pam)
%% clear and close all
% clear all
% close all
% clc

%% initial conditions and options
tspan = [0 1];
x0 = [1 0];
options = odeset('RelTol',1e-6,'AbsTol',1e-6);

%% solve process
equation = @(t,x)catalyst(t,x,u_pam);
[T,X] = ode45(equation,tspan,x0,options);
u_vect = arrayfun(@(t) control_fun(t,u_pam),T);

x = 1-X(:,1)-X(:,2);
performance_index = x(end);


%% vizualization
figure
plot(T,X,T,x);
title(sprintf('e = %.2f, f = %.2f, g = %.2f',u_pam(5),u_pam(6),u_pam(7)))
legend('x_1','x_2','x_3',2);
xlabel('time (s)');
ylabel('x');
grid on;

figure
plot(T,u_vect)
title(sprintf('a = %.2f, b = %.2f, c = %.2f, d = %.2f',u_pam(1),u_pam(2),u_pam(3),u_pam(4)))
xlabel('time (s)');
ylabel('u');
grid on;
end

