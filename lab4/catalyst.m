function [dx,u] = catalyst(t,x,u_pam)

u = control_fun(t,u_pam);

dx = zeros(2,1);
dx(1) = u*(10*x(2)-x(1));
dx(2) = u*(x(1)-10*x(2))-(1-u)*x(2);
end