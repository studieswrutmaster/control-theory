function [performance_index] = sim_model1(u_values)

periods = length(u_values);


u_min = 0;
u_max = 12;

t0 = 0;
tF = 61.17;

dt = tF/periods;
penalty = 10^3;

tF = dt;

x0 = [1 150 0 10];

T_vect = [];
X_vect = [];
u_vect = [];

for i = 1:periods
    tspan = [t0 tF];
    u = u_values(i);
    if u < u_min 
        u = u_min;
    elseif u > u_max
        u = u_max;
    end
    equation = @(t,x)model(t,x ,u);
    [T,X] = ode45(equation,tspan,x0);
    x0 = X(end,1:4);
    t0 = tF;
    tF = tF +dt;
    
    u_vect = [u_vect u*ones(1,length(T))];
    T_vect = [T_vect T'];
    X_vect = [X_vect X'];
end


performance_index = X_vect(3,end)*X_vect(4,end) - penalty*(X_vect(4,end)-200)^2;
performance_index = -performance_index;

end