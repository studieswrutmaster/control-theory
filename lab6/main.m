clc 
clear all
close all

%% initial parameters
u_values = zeros(10,1);

opts=optimset('Display','iter-detailed');
u_opt = fminunc(@sim_model1,u_values,opts);
sim_model(u_opt);